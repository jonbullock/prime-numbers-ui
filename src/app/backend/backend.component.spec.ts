import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackendComponent } from './backend.component';

describe('BackendComponent', () => {
  let component: BackendComponent;
  let fixture: ComponentFixture<BackendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render structure', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('div.panel div.panel-body')?.textContent).toContain('Maximum number');
    expect(compiled.querySelector('div.panel div.panel-body button')?.textContent).toContain('Generate Prime Numbers');
  });
});
