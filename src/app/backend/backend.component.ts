import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

const endpoint = 'http://localhost:8080/primes';

@Component({
  selector: 'app-backend',
  templateUrl: './backend.component.html',
  styleUrls: ['./backend.component.css']
})
export class BackendComponent implements OnInit {
  upperValue: number = 5;
  isLoading: boolean = false;
  totalRows: number = 0;
  pageSize: number = 5;
  currentPage: number = 0;
  pageSizeOptions: number[] = [5, 10, 25];
  primeNumbers!: number[];
  displayedColumns: string[] = ['primenumber'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  errorMessage: string = "";

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  loadData() {
    if (!isNaN(this.upperValue)) {
      this.errorMessage = "";
      this.isLoading = true;
      let offset = this.currentPage * this.pageSize;
      let URL = endpoint + '?upperValue=' + this.upperValue + '&offset=' + offset + '&pageSize=' + this.pageSize;

      fetch(URL)
        .then(response => response.json())
        .then(data => {
          this.dataSource.data = data.primeNumbers;
          setTimeout(() => {
            this.paginator.pageIndex = this.currentPage;
            this.paginator.length = data.total;
          });
          this.isLoading = false;
        }, error => {
          console.log(error);
          this.errorMessage = "Error occurred communicating with Prime Numbers API, see console for more details."
          this.isLoading = false;
        });
    } else {
      this.errorMessage = "Supplied value is not a number";
    }
  }

  pageChanged(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
    this.loadData();
  }
}
